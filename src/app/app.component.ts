import {Component, OnInit} from '@angular/core';
import {FeedService} from "./shared/FeedService/feed-service.service";
declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: []
})
export class AppComponent implements OnInit {
  private feedUrl: string = 'http://ww2-weapons.com/feed/';
  private feed: any = {};
  err: any;

  constructor(private feedService: FeedService) {

  }

  public storeAndDownload() {
    $('a.downloadImg').get(0).click();
    this.store();
  }

  public store() {
    this.selectElementText($('article')[0], window);
    document.execCommand('copy');
    this.clearSelection();
    alert('Article text copied to clipboard.');
  }

  public selectElementText(el, win) {
    win = win || window;
    var doc = win.document, sel, range;
    try {
      if (win.getSelection && doc.createRange) {
        sel = win.getSelection();
        range = doc.createRange();
        range.selectNodeContents(el);
        sel.removeAllRanges();
        sel.addRange(range);
      } else if (doc.body.createTextRange) {
        range = doc.body.createTextRange();
        range.moveToElementText(el);
        range.select();
      }
    } catch (e) {
      console.log(e);
    }
  }

  public clearSelection() {
    if (window.getSelection) window.getSelection().removeAllRanges();
  }

  private refreshFeed() {
    this.feedService.getFeedContent(this.feedUrl)
      .subscribe(
        feed => this.feed = feed,
        error => console.log(error));
  }

  ngOnInit() {
    this.refreshFeed();
  }
}
