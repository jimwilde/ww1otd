import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {CommonModule} from "@angular/common";


import {AppComponent} from './app.component';
import {FeedComponent} from "./shared/feed.component";
import {FeedCardComponent} from './feed-card/feed-card.component';
import {StripHtmlTagsPipe} from './pipe/strip-html-tags.pipe';
import {MdCardModule} from "@angular2-material/card";
import {MdToolbarModule} from "@angular2-material/toolbar";
import {MdIconModule, MdIconRegistry} from "@angular2-material/icon";
import {MdButtonModule} from "@angular2-material/button";
import {FeedService} from "./shared/FeedService/feed-service.service";

@NgModule({
  declarations: [
    AppComponent,
    FeedComponent,
    FeedCardComponent,
    StripHtmlTagsPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    CommonModule,
    MdCardModule,
    MdToolbarModule,
    MdButtonModule,
    MdIconModule
  ],
  providers: [FeedService, MdIconRegistry],
  bootstrap: [AppComponent]
})
export class AppModule {
}
