import {Http} from "@angular/http";
import {Component, Input} from "@angular/core/src/metadata/directives";
import {OnInit} from "@angular/core";

@Component({
    selector: 'feed',
    template: `
		<div class="feed">
	      <h1>{{data?.title}}</h1>
	      <h3>{{data?.url}}</h3>
	      <ul>
	        <li *ngFor="let entry of data?.entries">
	          <a href="{{entry.link}}">
	            {{entry.title}}
	          </a>
	          <div class="e2e-trusted-html" [innerHtml]="entry.content"></div>
	        </li>
	      </ul>
	    </div>
	`
})
export class FeedComponent implements OnInit {
    @Input() url;
    data:any;

    constructor(private http:Http) {
    }

    ngOnInit() {
        this.http.get(this.url)
            .subscribe(res => {
                this.data = res.json();
            });
    }
}
