import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import { Feed } from './feed';
import * as moment from 'moment';
declare var $:any;

@Injectable()
export class FeedService {

  private rssToJsonServiceBaseUrl: string = 'https://rss2json.com/api.json?rss_url=';

  constructor(
    private http: Http
  ) { }

  getFeedContent(url: string): Observable<Feed> {
    return this.http.get(this.rssToJsonServiceBaseUrl + url)
      .map(this.formatData)
      .catch(this.handleError);
  }

  public formatData(postData):any {
    let feed = postData.json().items[1],
      content = $(feed.content),
      imgText = content.children('.wp-caption-text').text(),
      href = content.children('.alignright a').attr('href'),
      imgOrigSrc = content.find('img').attr('src');
    return {
      today: moment().format('dddd MMMM D, YYYY'),
      yesteryear: moment(new Date(feed.title.replace('Diary ', ''))).format('dddd MMMM D, YYYY'),
      imgOrigSrc: imgOrigSrc,
      imgSrc: href,
      imgText: imgText,
      filename: imgOrigSrc.substring(0, imgOrigSrc.indexOf('?')),
      content: $('<div></div>').append($(content).slice(5)).html(),
      link: feed.link,
      pubDate: moment(new Date(feed.pubDate)).format('DD/MM/Y')
    };
  }

  private handleError (error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }
}
