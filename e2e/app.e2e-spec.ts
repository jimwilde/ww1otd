import { Ww1Page } from './app.po';

describe('ww1 App', function() {
  let page: Ww1Page;

  beforeEach(() => {
    page = new Ww1Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
